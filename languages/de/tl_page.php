<?php
$GLOBALS['TL_LANG']['tl_page']['extra_legend']   = 'Bilder der Seite: Hintergrund und Navigation';

$GLOBALS["TL_LANG"]["tl_page"]["ixPageImage"]=array(
	'Hintergrundsbild-Bild', '/tl_files/userImages/banner'
);

$GLOBALS['TL_LANG']['tl_page']['ixPageNaviImage'] = array('Bild für Top-Navigation', '/tl_files/userImages/navigation');
$GLOBALS['TL_LANG']['tl_page']['ixPageNaviText'] = array('Text für Top-Navigation', 'Ein bis zwei kurze Sätze');


?>
# zixBackendPics

* Copyright: Ixtensa (http://wwww.ixtensa.de)
* Author: Siegi Genschow (s.genschow@ixtensa.de)
* License: LGPL

### System requirements
* Contao >=3.4

### Description
Enables you to add an image to every page's settings. This image can be used as you demand when the frontend is generated (e.g. page background)

### Getting Started
Add an image to the page's settings. To use it image as page background edit the fe_page template and include the image as background with inline CSS using the page object ($objPage->ixPageImage) or by using the insert-tag {{ix_page_back_image}}. If you want to insert the parent's image you can use the insert-tag {{ix_page_back_image::parent}}.

** Example: **
> <body style="background: url({{ix_page_back_image}}) no-repeat top center">
> ...
> </body>
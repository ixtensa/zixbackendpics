<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2013 Leo Feyer
 *
 * @package Zzz_ixtensa_core
 * @link    https://contao.org
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 */


/**
 * Register the namespaces
 */
ClassLoader::addNamespaces(array
(
	'IXTENSA',
));


/**
 * Register the classes
 */
ClassLoader::addClasses(array
(
	// Classes
	'IXTENSA\zixBackendPagePicsHooks' => 'system/modules/zixBackendPagePics/classes/zixBackendPagePicsHooks.php',
));

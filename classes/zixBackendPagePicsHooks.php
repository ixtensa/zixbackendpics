<?php
/**
 * Namespace
 */
namespace IXTENSA;

class zixBackendPagePicsHooks extends \Backend {
	// ++++++++++++++++++++++++ HOOKS ++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// Hook: eigene Insert-Tags
	public function replaceInsertTags($strTag, $blnCache=false) {
		$tag = $this->parseTag($strTag);
		$strTag = $tag[0];
		
		// gibt Style-Eigenschaft der aktuellen Seite zurück
		// ix_page_back_image / ix_page_back_image::default:/tl_files/foo.jpg / ix_page_back_image::parent / ix_page_back_image::parent:default:foo.jpg
		if($strTag == "ix_page_back_image") {
			$isParent = in_array("parent", $tag); // nach einem Bild der übergeordneten Seite suchen
			$defaultPath = false; // Bild, das angezeigt wird, wenn der Seite kein (bzw. übergeord.) ixPageImage zugeordnet ist
			foreach($tag as $k=>$v) {
				if($v=="default") {
					$defaultPath = isset($tag[$k+1]) ? $tag[$k+1] : false;
					break;	
				}	
			} 

			$objPage = $GLOBALS['objPage'];
			$pageImg = \FilesModel::findByUuid($objPage->ixPageImage)->path;
		
			if( empty($pageImg) ) {
				if( $isParent ) {
					$imgs = $this->getParentImages($objPage->id);
					foreach($imgs as $img) {
						if(!empty($img)) return $img;	
					}
				}
				if( $defaultPath ) return $defaultPath;
				return '';
			} else return $pageImg;
		}

		return false; // Tag nicht bekannt
	}
	
	// +++++++++++++++++++++++++++++++++ HELPER +++++++++++++++++++++++++++++++++++++++++++++
	// Mixed in einen String packen
	protected function debug($a) {
		return print_r($a, 1);
	}

	// rekursiv alle ParentImages abrufen
	protected function getParentImages($id) {
		$data = $this->_getParentImages($id);
		array_shift($data);
		return $data; // das erste Element wird nicht benötigt
	}
	protected function _getParentImages($id, $data=array()) {
		$sql = "SELECT pid, ixPageImage FROM tl_page WHERE id=?";
		$this->import('Database');
		$result = $this->Database->prepare($sql)->execute($id);
		$arr = $result->fetchAllAssoc();
		if( count($arr)>0 ) {
			$data[] = \FilesModel::findByUuid($arr[0]['ixPageImage'])->path;	
			$id = $arr[0]['pid'];
			$data = $this->_getParentImages($id, $data);
		}
		return $data;	
	}

	// das inserttag zerlegen und als array zurueckgeben:
	// foo::goo:moo -> array(foo,goo,moo)
	protected function parseTag($tag) {
		$result = array();
		
		$t = explode("::", $tag);
		
		// ist :: vorhanden?
		if(count($t)>=2) {
			$result[0]=$t[0];
			
			// sind : vorhanden?
			$t2 = explode(":", $t[1]);
			
			if(count($t2)>=2) {
				$result = array_merge($result, $t2);
			} else {
				$result[1]=$t[1];
			}
		} else {
			$result = array($tag);
		}
		return $result;
	}
	
} // Hooks
?>